pub mod exported;
pub mod partitions;
pub mod ufs;

#[cfg(test)]
mod tests;

use modular_bitfield::prelude::*;

#[bitfield(bits = 64)]
#[derive(Debug, Clone)]
pub struct SlotInfo {
    #[skip]
    __: B48,
    // slot priority
    priority: B2, // Bits 48,49
    is_active: B1, // Bit 50
    retry_count: B3, // Bits 51,52,53
    boot_successful: B1, // Bit 54
    is_unbootable: B1,   // Bit 55
    #[skip]
    __: B8

}

pub trait BootSlot {
    fn from_boot_flags(flags: u64) -> SlotInfo;
    fn get_boot_flags(&self) -> u64;
}

impl BootSlot for SlotInfo {
    fn from_boot_flags(flags: u64) -> SlotInfo {
        Self::from_bytes(flags.to_le_bytes())
    }

    fn get_boot_flags(&self) -> u64 {
        u64::from_le_bytes(self.clone().into_bytes())
    }
}