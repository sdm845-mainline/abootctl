use std::fs::write;

// Unimplemented for now
pub fn set_ufs_slot(slot: i32) {
    match write("/sys/class/FIXME", slot.to_string()) {
        Ok(_) => (),
        Err(e) => panic!("{}", e)
    }
}
