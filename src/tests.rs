/*
// Currently broken! Needs rewriting

#[cfg(test)]
mod test {
    use super::*;
    // Test bit map
    #[test]
    fn test_from_boot_flags() {
        let slot = SlotInfo::from_boot_flags(
            0b0000000011000100000000000000000000000000000000000000000000000000,
        );
        //  10000000000000000000000000000000000000000000000000000000
        //  ^ bit 55
        println!("{:?}", slot);
        assert_eq!(slot.boot_successful(), 1);
        assert_eq!(slot.is_unbootable(), 1);
        assert_eq!(slot.is_active(), 1);

        // Inverted check
        let slot = SlotInfo::from_boot_flags(
            0b1111111100111011111111111111111111111111111111111111111111111111,
        );
        //  10000000000000000000000000000000000000000000000000000000
        //  ^ bit 55
        println!("{:?}", slot);
        assert_eq!(slot.boot_successful(), 0);
        assert_eq!(slot.is_unbootable(), 0);
        assert_eq!(slot.is_active(), 0);
    }

    #[test]
    fn test_set_slot() {
        let devs: Vec<PathBuf> = vec![PathBuf::from("./images/sde.img")];
        // sde image is on slot 1
        set_slot(0, devs.clone());
        assert_eq!(get_current_slot(devs.clone()), 0);
        set_slot(1, devs.clone());
        assert_eq!(get_current_slot(devs), 1);
    }
}
*/
