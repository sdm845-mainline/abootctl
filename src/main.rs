//#![allow(dead_code)]

use sdm845_abootctl::exported::*;
use std::{path::PathBuf, process};
use clap::{App, AppSettings, Arg, SubCommand};

fn main() {
    // CLI stuff
    let matches = App::new("abootctl")
        .version("1.0.0")
        .author("Aissa Z. B. <aissa.zenaida@pm.me>, Caleb C. <caleb@connolly.tech>")
        .about("Bootloader control for SDM845 OnePlus devices. CLI arguments compatible with Android bootctl. THIS MAY BRICK YOUR DEVICE - USE WITH CARE")
        .arg(Arg::with_name("DEV_DIR")
            .required(false)
            .help("Operate on image files within the target directory instead of operating on block devices"))
        .setting(AppSettings::ArgRequiredElseHelp)
        .subcommand(SubCommand::with_name("hal-info")
            .about("Show info about boot_control HAL used")
            .display_order(1))
        .subcommand(SubCommand::with_name("get-number-slots")
            .about("Prints number of slots")
            .display_order(2))
        .subcommand(SubCommand::with_name("get-current-slot")
            .about("Prints currently running SLOT")
            .display_order(3))
        .subcommand(SubCommand::with_name("mark-boot-successful")
            .about("Mark current slot as GOOD")
            .display_order(4))
        .subcommand(SubCommand::with_name("set-active-boot-slot")
            .about("On next boot, load and execute SLOT")
            .arg(Arg::with_name("SLOT")
                .required(true)
                .index(1)
                .possible_values(&["0", "1"]))
            .display_order(5))
        .subcommand(SubCommand::with_name("set-slot-as-unbootable")
            .about("Mark SLOT as invalid")
            .arg(Arg::with_name("SLOT")
                .required(true)
                .index(1)
                .possible_values(&["0", "1"]))
            .display_order(6))
        .subcommand(SubCommand::with_name("is-slot-bootable")
            .about("Returns 0 only if SLOT is bootable")
            .arg(Arg::with_name("SLOT")
                .required(true)
                .index(1)
                .possible_values(&["0", "1"]))
            .display_order(7))
        .subcommand(SubCommand::with_name("is-slot-marked-successful")
            .about("Returns 0 only if SLOT is marked GOOD")
            .arg(Arg::with_name("SLOT")
                .required(true)
                .index(1)
                .possible_values(&["0", "1"]))
            .display_order(8))
        .subcommand(SubCommand::with_name("get-suffix")
            .about("Prints suffix for SLOT")
            .arg(Arg::with_name("SLOT")
                .required(true)
                .index(1)
                .possible_values(&["0", "1"]))
            .display_order(9))
        .get_matches();

    let custom_devpath = PathBuf::from(matches.value_of("DEV_DIR").unwrap_or(""));
    let mut devs: Vec<PathBuf> = Vec::new();
    if custom_devpath.parent().is_some() {
        let paths = custom_devpath.read_dir().unwrap();
        for path in paths {
            devs.push(path.unwrap().path());
        }
    }

    match matches.subcommand_name() {
        Some("hal-info") => {
            println!("HAL Version: linux.hardware.boot@1.0::abootctl");
        }
        Some("get-number-slots") => {
            /* if let (_, _, _) = partitions::get_boot_partitions() { */
            println!("2"); /* } else { println!("1"); } */
        }
        Some("get-current-slot") => {
            println!("{}", get_current_slot(devs));
        }
        Some("mark-boot-successful") => {
            mark_successful(get_current_slot(devs.clone()), devs);
        }
        Some("set-active-boot-slot") => {
            set_slot(
                matches
                    .subcommand_matches("set-active-boot-slot")
                    .unwrap()
                    .value_of("SLOT")
                    .unwrap()
                    .parse::<i32>()
                    .unwrap(),
                devs,
                true,
            );
        }
        Some("set-slot-as-unbootable") => {
            mark_unbootable(
                matches
                    .subcommand_matches("set-slot-as-unbootable")
                    .unwrap()
                    .value_of("SLOT")
                    .unwrap()
                    .parse::<i32>()
                    .unwrap(),
                devs,
            );
        }
        Some("is-slot-bootable") => {
            process::exit(is_bootable(
                matches
                    .subcommand_matches("is-slot-bootable")
                    .unwrap()
                    .value_of("SLOT")
                    .unwrap()
                    .parse::<i32>()
                    .unwrap(),
                devs,
            ) as i32);
        }
        Some("is-slot-marked-successful") => {
            process::exit(is_successful(
                matches
                    .subcommand_matches("is-slot-marked-successful")
                    .unwrap()
                    .value_of("SLOT")
                    .unwrap()
                    .parse::<i32>()
                    .unwrap(),
                devs,
            ) as i32);
        }
        Some("get-suffix") => {
            println!(
                "{}",
                get_suffix(
                    matches
                        .subcommand_matches("get-suffix")
                        .unwrap()
                        .value_of("SLOT")
                        .unwrap()
                        .parse::<i32>()
                        .unwrap(),
                )
            );
        }
        _ => {
            process::exit(64);
        } // Android does it this way idk
    }
}

